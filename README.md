STEPS:

1. Clone the project
2. Build project --> mvn clean install
3. Create database kals_business on mysql, and import file kals_business.sql
4. go to Kals Business API folder --> cd kals-business-api
5. run api using mvn spring-boot:run
