-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3307
-- Generation Time: Sep 14, 2020 at 12:30 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kals_business`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` text,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` timestamp NULL DEFAULT NULL,
  `create_ip` varchar(50) NOT NULL,
  `modified_ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `value`, `status`, `create_by`, `modified_by`, `create_at`, `modified_at`, `create_ip`, `modified_ip`) VALUES
('6702b0a6-a6ae-4abd-b0d4-4ae53b9bd41c', 'GOLONGAN', 'THT', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL),
('7c8a7e9b-e61f-4413-befe-d0323a2c4519', 'GOLONGAN', 'KANDUNGAN_GINEKOLOGI', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL),
('90b04105-3aec-4d02-8ad8-3be232d434b8', 'GOLONGAN', 'UMUM', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL),
('aac022cf-d222-4bb6-a6ca-2a2a0fecdbc0', 'GOLONGAN', 'PSIKIATER', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL),
('e72d1c93-7f62-4d3e-ab6e-b79068611537', 'GOLONGAN', 'ANAK', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL),
('ee6fc823-776c-4fd0-8604-aca959bb3a77', 'GOLONGAN', 'GIGI_MULUT', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_type`
--

CREATE TABLE `category_type` (
  `id` varchar(50) NOT NULL,
  `category_id` varchar(50) NOT NULL,
  `type_id` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` timestamp NULL DEFAULT NULL,
  `create_ip` varchar(50) NOT NULL,
  `modified_ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_type`
--

INSERT INTO `category_type` (`id`, `category_id`, `type_id`, `status`, `create_by`, `modified_by`, `create_at`, `modified_at`, `create_ip`, `modified_ip`) VALUES
('2f356e64-412c-4e41-b7c5-1f4af395df12', '6702b0a6-a6ae-4abd-b0d4-4ae53b9bd41c', '3e2fb300-b4b8-4264-bedc-8eeef12da312', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL),
('7c2d6e30-4318-435c-bccf-95b172ac2956', '90b04105-3aec-4d02-8ad8-3be232d434b8', '3e2fb300-b4b8-4264-bedc-8eeef12da312', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL),
('cb6db975-66e8-4a21-b5b1-26e9590f53bb', '90b04105-3aec-4d02-8ad8-3be232d434b8', '4410d8f2-061e-47ed-86a9-74f93b3209ef', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL),
('cffadb4e-9737-4005-8118-93b6d036697c', '90b04105-3aec-4d02-8ad8-3be232d434b8', 'fe0ee4e7-8018-4c13-80da-f23d3f43cfe9', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL),
('d6d25e9b-b82e-4098-ba7a-4d689b7d3b52', 'ee6fc823-776c-4fd0-8604-aca959bb3a77', 'ee6ac870-1f40-4fea-b537-dcf7bf8c0090', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL),
('f6a44d88-d438-49bb-b966-ef6c5ca9c0d7', 'ee6fc823-776c-4fd0-8604-aca959bb3a77', '3e2fb300-b4b8-4264-bedc-8eeef12da312', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.01', NULL),
('f7899b68-64f1-48ee-92af-2a1129f72b5d', '90b04105-3aec-4d02-8ad8-3be232d434b8', 'ee6ac870-1f40-4fea-b537-dcf7bf8c0090', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` varchar(50) NOT NULL,
  `personal_report_id` varchar(50) NOT NULL,
  `payment_method_id` varchar(50) NOT NULL,
  `payment_date` datetime NOT NULL,
  `total` double NOT NULL,
  `response_final` text,
  `description` text,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` timestamp NULL DEFAULT NULL,
  `create_ip` varchar(50) NOT NULL,
  `modified_ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment_method`
--

CREATE TABLE `payment_method` (
  `id` varchar(50) NOT NULL,
  `type` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` timestamp NULL DEFAULT NULL,
  `create_ip` varchar(50) NOT NULL,
  `modified_ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `personal`
--

CREATE TABLE `personal` (
  `id` varchar(50) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT '',
  `nickname` varchar(50) NOT NULL DEFAULT '',
  `gender` varchar(1) NOT NULL DEFAULT '' COMMENT 'M = Male\r\nL = Laki - Laki\r\nF = Female\r\nP = Perempuan',
  `marital_status` varchar(1) DEFAULT '' COMMENT 'M = MarriedS = SingleD = DivorcedW = Widowed',
  `religion` varchar(20) NOT NULL COMMENT 'ISLAM,HINDU,BUDHA,KRISTEN PROTESTAN,KATOLIK, KONGHUCU',
  `birth_date` date DEFAULT NULL,
  `place_of_birth` varchar(50) NOT NULL,
  `mother_name` varchar(50) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` timestamp NULL DEFAULT NULL,
  `create_ip` varchar(50) NOT NULL,
  `modified_ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `personal`
--

INSERT INTO `personal` (`id`, `first_name`, `last_name`, `nickname`, `gender`, `marital_status`, `religion`, `birth_date`, `place_of_birth`, `mother_name`, `status`, `create_by`, `modified_by`, `create_at`, `modified_at`, `create_ip`, `modified_ip`) VALUES
('742d071b-6c97-4f34-ba53-bf3bad94a482', 'kals', 'mak', 'mak', 'M', 'S', 'ISLAM', '1996-04-09', 'Lumajang', 'k', 1, 'system', 'system', '2020-09-14 15:05:34', '2020-09-14 08:05:34', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1'),
('cf84b01b-ae08-4415-8bba-b7ca1b0ba212', 'kals', 'mak', 'mak', 'M', 'S', 'ISLAM', '1996-04-09', 'Lumajang', 'k', 1, 'system', 'system', '2020-09-14 15:10:21', '2020-09-14 08:10:21', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1');

-- --------------------------------------------------------

--
-- Table structure for table `personal_report`
--

CREATE TABLE `personal_report` (
  `id` varchar(50) NOT NULL,
  `personal_visitor_id` varchar(50) NOT NULL,
  `personal_employee_id` varchar(50) DEFAULT NULL,
  `personal_respondent_id` varchar(50) DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `report` text NOT NULL,
  `respon` text NOT NULL,
  `description` text,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` timestamp NULL DEFAULT NULL,
  `create_ip` varchar(50) NOT NULL,
  `modified_ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `personal_report_detail`
--

CREATE TABLE `personal_report_detail` (
  `id` varchar(50) NOT NULL,
  `personal_report_id` varchar(50) NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `cost` double NOT NULL,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` timestamp NULL DEFAULT NULL,
  `create_ip` varchar(50) NOT NULL,
  `modified_ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `personal_user_type`
--

CREATE TABLE `personal_user_type` (
  `id` varchar(50) NOT NULL,
  `personal_id` varchar(50) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `category_type_id` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` timestamp NULL DEFAULT NULL,
  `create_ip` varchar(50) NOT NULL,
  `modified_ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `personal_user_type`
--

INSERT INTO `personal_user_type` (`id`, `personal_id`, `user_id`, `category_type_id`, `status`, `create_by`, `modified_by`, `create_at`, `modified_at`, `create_ip`, `modified_ip`) VALUES
('1fe0dc55-f78b-4057-ab54-2a6bc7f18ac0', '742d071b-6c97-4f34-ba53-bf3bad94a482', 'f758f784-a217-48b5-8991-e35fb7e0c69b', 'cb6db975-66e8-4a21-b5b1-26e9590f53bb', 1, 'system', NULL, '2020-09-14 15:05:34', '2020-09-14 08:05:34', '0:0:0:0:0:0:0:1', NULL),
('942a4a43-1e1b-4f16-9d51-2bfb4add8fc2', 'cf84b01b-ae08-4415-8bba-b7ca1b0ba212', '56416859-91ab-4d47-8755-ec63b39355ca', 'cb6db975-66e8-4a21-b5b1-26e9590f53bb', 1, 'system', NULL, '2020-09-14 15:10:21', '2020-09-14 08:10:21', '0:0:0:0:0:0:0:1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` varchar(50) NOT NULL,
  `parent_id` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` timestamp NULL DEFAULT NULL,
  `create_ip` varchar(50) NOT NULL,
  `modified_ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `parent_id`, `name`, `description`, `status`, `create_by`, `modified_by`, `create_at`, `modified_at`, `create_ip`, `modified_ip`) VALUES
('1eff9423-4cd3-4b1d-bcec-f5306896a157', NULL, 'VISITOR_OFFLINE', NULL, 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL),
('79adc238-63a7-4ab2-8049-49f71d198405', NULL, 'FRONT_OFFICE', NULL, 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` text,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` timestamp NULL DEFAULT NULL,
  `create_ip` varchar(50) NOT NULL,
  `modified_ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `name`, `value`, `status`, `create_by`, `modified_by`, `create_at`, `modified_at`, `create_ip`, `modified_ip`) VALUES
('0ebe90b1-6b21-483d-80cc-260d986da370', 'EMPLOYEE', 'SECURITY', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL),
('3e2fb300-b4b8-4264-bedc-8eeef12da312', 'EMPLOYEE', 'DOCTOR', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.01', NULL),
('4410d8f2-061e-47ed-86a9-74f93b3209ef', 'VISITOR', 'VISITOR_OFFLINE', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL),
('ee6ac870-1f40-4fea-b537-dcf7bf8c0090', 'EMPLOYEE', 'FRONT_OFFICE', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL),
('fe0ee4e7-8018-4c13-80da-f23d3f43cfe9', 'VISITOR', 'VISITOR_ONLINE', 1, 'mak', NULL, '2020-09-14 00:00:00', NULL, '127.0.0.1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` varchar(50) NOT NULL,
  `username` text,
  `password` text,
  `salt` text,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` timestamp NULL DEFAULT NULL,
  `create_ip` varchar(50) NOT NULL,
  `modified_ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `salt`, `status`, `create_by`, `modified_by`, `create_at`, `modified_at`, `create_ip`, `modified_ip`) VALUES
('56416859-91ab-4d47-8755-ec63b39355ca', 'maks@gmail.com', '$6$UwfHvdcxEjzjfyb9$XPtOzqNtih9BgV4EZ5dephMfogTnDjsofepC9VVWow1FjrkFIM4nge2WjsllB18nOTx7UWoE6qPniw3P9w4Fx.', '$6$UwfHvdcxEjzjfyb9$', 0, 'system', 'system', '2020-09-14 15:10:21', '2020-09-14 08:10:21', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1'),
('f758f784-a217-48b5-8991-e35fb7e0c69b', 'mak@gmail.com', '$6$ZEBaPJbwUNTk26ew$oI2lUYwgoCAQUFW7PXvVM3GbnXGiwKQqUD9jr3FDIiJ/4ubNLc1BopBtFkNZx9RI.t2WjV5vfzuuXiBJoEFzT0', '$6$ZEBaPJbwUNTk26ew$', 0, 'system', 'system', '2020-09-14 15:05:34', '2020-09-14 08:05:34', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` varchar(50) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `role_id` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `modified_at` timestamp NULL DEFAULT NULL,
  `create_ip` varchar(50) NOT NULL,
  `modified_ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `user_id`, `role_id`, `status`, `create_by`, `modified_by`, `create_at`, `modified_at`, `create_ip`, `modified_ip`) VALUES
('55e48acd-dbb1-49b6-85f4-3c2252330b47', 'f758f784-a217-48b5-8991-e35fb7e0c69b', '1eff9423-4cd3-4b1d-bcec-f5306896a157', 1, 'system', NULL, '2020-09-14 15:05:34', '2020-09-14 08:05:34', '0:0:0:0:0:0:0:1', NULL),
('ee82984b-26e9-4a65-9ee6-a4000da86775', '56416859-91ab-4d47-8755-ec63b39355ca', '1eff9423-4cd3-4b1d-bcec-f5306896a157', 1, 'system', NULL, '2020-09-14 15:10:21', '2020-09-14 08:10:21', '0:0:0:0:0:0:0:1', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_type`
--
ALTER TABLE `category_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_CategoryToCategoryTyoe` (`category_id`),
  ADD KEY `FK_TypeToCategoryTyoe` (`type_id`) USING BTREE;

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_PersonalReportToPayment` (`personal_report_id`) USING BTREE,
  ADD KEY `FK_PaymentMethodToPayment` (`payment_method_id`) USING BTREE;

--
-- Indexes for table `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_report`
--
ALTER TABLE `personal_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_PersonalToPersonalReportVisitor` (`personal_visitor_id`) USING BTREE,
  ADD KEY `FK_PersonalToPersonalReportEmployee` (`personal_employee_id`) USING BTREE,
  ADD KEY `FK_PersonalToPersonalReportRespondent` (`personal_respondent_id`) USING BTREE;

--
-- Indexes for table `personal_report_detail`
--
ALTER TABLE `personal_report_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_PersonalReportToPersonalReportDetail` (`personal_report_id`) USING BTREE;

--
-- Indexes for table `personal_user_type`
--
ALTER TABLE `personal_user_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_CategoryTypeToPersonalUserType` (`category_type_id`) USING BTREE,
  ADD KEY `FK_PersonalToPersonalUserType` (`personal_id`) USING BTREE,
  ADD KEY `FK_UserToPersonalUserType` (`user_id`) USING BTREE;

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_UserToUserRole` (`user_id`) USING BTREE,
  ADD KEY `FK_RoleToUserRole` (`role_id`) USING BTREE;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category_type`
--
ALTER TABLE `category_type`
  ADD CONSTRAINT `category_type_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`),
  ADD CONSTRAINT `category_type_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_method` (`id`),
  ADD CONSTRAINT `payment_ibfk_2` FOREIGN KEY (`personal_report_id`) REFERENCES `personal_report` (`id`);

--
-- Constraints for table `personal_report`
--
ALTER TABLE `personal_report`
  ADD CONSTRAINT `personal_report_ibfk_1` FOREIGN KEY (`personal_visitor_id`) REFERENCES `personal` (`id`),
  ADD CONSTRAINT `personal_report_ibfk_2` FOREIGN KEY (`personal_employee_id`) REFERENCES `personal` (`id`),
  ADD CONSTRAINT `personal_report_ibfk_3` FOREIGN KEY (`personal_respondent_id`) REFERENCES `personal` (`id`);

--
-- Constraints for table `personal_report_detail`
--
ALTER TABLE `personal_report_detail`
  ADD CONSTRAINT `personal_report_detail_ibfk_1` FOREIGN KEY (`personal_report_id`) REFERENCES `personal_report` (`id`);

--
-- Constraints for table `personal_user_type`
--
ALTER TABLE `personal_user_type`
  ADD CONSTRAINT `personal_user_type_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `personal_user_type_ibfk_2` FOREIGN KEY (`personal_id`) REFERENCES `personal` (`id`),
  ADD CONSTRAINT `personal_user_type_ibfk_3` FOREIGN KEY (`category_type_id`) REFERENCES `category_type` (`id`);

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
